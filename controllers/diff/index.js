const path = require('path');
const ENV = require('../../etc/path');

const lib = require('gitarchive_lib');
const APIError = lib.error;
const APISuccess = lib.success;
const Git = lib.git;

/**
 * Handle queries to return diff of two commits from a specific resource.
 * 
 * @param: 	resource_id (mandatory),
 * 			from_commit_id (mandatory),
 * 			to_commit_id (mandatory)
 * @return: diff object (json)
 */

module.exports = {

	getBody: function (req, res, next) {

		return getDiff (req, res, next, "body");
	},

	getHeaders: function (req, res, next) {

		return getDiff (req, res, next, "headers");
	},
	
};

function getDiff (req, res, next, diffType) {

	// Controller's params
    const 	resource_id = req.params.resource_id,
			from_commit_id = req.params.from_commit_id,
			to_commit_id = req.params.to_commit_id;
    
    // Validate resource & commit params
    const hashFormat = new RegExp("^[A-z0-9]+$");
    
    if (!hashFormat.test(resource_id))
        { return next(APIError.badRequest("Resource hash is malformed.")); }
	if (!hashFormat.test(from_commit_id))
		{ return next(APIError.badRequest("Resource first commit hash is malformed.")); }
	if (!hashFormat.test(to_commit_id))
		{ return next(APIError.badRequest("Resource first second hash is malformed.")); }
	
	if (from_commit_id === to_commit_id)
		{ return next(APIError.badRequest("From-commit's id and to-commit's id are equal.")); }

    // 1. Load repo
    // 2. Fetch raw data from a specific commit.
    // 3. Return raw data if everything ok.
    // 4. Log error otherwise :|
    
    (new Promise (function(resolve, reject) {
        const repo = new Git(path.join(ENV.USER_CONTENT_PATH, resource_id))
		// TODO. Confirm that reject error is well-handled.
		if (!repo) { reject(APIError.notFound("Resource was not found.")); }
        else { resolve(repo); };
    }))
    .then((repo) => { return repo.diff(from_commit_id, to_commit_id, (diffType == "headers" ? "headers" : "body")); })
	.then((stdio) => {
		
		// TODO. Handle stdio output errors.
		// Handle Git lib errors
		if (stdio.stderr) { throw new Error(stdio.stderr); }

		// If stdio is empty, return empty chunks array
		if (stdio.stdout == "") { return []; }

		// Work with proper result
		let diff = stdio.stdout.replace("\r\n", "\n").replace("\r", "\n").split("\n");
		
		if (diff[0].slice(0,4) !== "diff")
			{ console.log("Error at diff[0]: '" + diff[0] + "'"); return next(APIError.notImplemented("Not implemented diff format.")); }
		if (diff[1].slice(0,5) !== "index")
			{ console.log("Error at diff[1]: '" + diff[1] + "'"); return next(APIError.notImplemented("Not implemented diff format.")); }
		if (diff[2].slice(0,3) !== "---")
			{ console.log("Error at diff[2]: '" + diff[2] + "'"); return next(APIError.notImplemented("Not implemented diff format.")); }
		if (diff[3].slice(0,3) !== "+++")
			{ console.log("Error at diff[3]: '" + diff[3] + "'"); return next(APIError.notImplemented("Not implemented diff format.")); }	
		
		// Diff output format generally end with a "\n".
		// If so, last element shall be removed.
		if (diff[diff.length-1] == "") { diff.pop(); }
		
		// Simple function to reset a chunk
		function resetChunk () { 
			return {
				"object": "chunk",
				"from": { "start_line": null, "lines": null},
				"to": { "start_line": null, "lines": null},
				"lines": []
			};
		}

		// Loop through diff's chunks
		let i = 4, preline, postline;
		let chunks = [], chunk;
		
		do {
			
			if (diff[i] && diff[i].slice(0,2) == "@@") {

				if (typeof chunk !== "undefined")
					{ chunks.push(chunk); }
				
				chunk = resetChunk();

				const chunkHeader = diff[i].split('@@')[1];
				const chunkHeaderRegExp = /^ \-([0-9]+)(,([0-9]+))? \+([0-9]+)(,([0-9]+))? $/g;
				let match = chunkHeaderRegExp.exec(chunkHeader);
				
				if (match && match[1]) {
					chunk.from.start_line = match[1] * 1;
					chunk.from.lines = typeof match[3] == "undefined" ? 0 : match[3] * 1;
					chunk.to.start_line = match[4] * 1;
					chunk.to.lines = typeof match[6] == "undefined" ? 0 : match[6] * 1;

					preline = chunk.from.start_line;
					postline = chunk.to.start_line;
				}
			}

			else if (diff[i].slice(0,1) == " ") {
				chunk.lines.push({
					"status": "unchanged",
					"text": diff[i].slice(1),
					"preline": (preline++),
					"postline": (postline++)
				});
			}

			else if (diff[i].slice(0,1) == "-") {
				chunk.lines.push({
					"status": "deletion",
					"text": diff[i].slice(1),
					"preline": (preline++),
					"postline": null
				});
			}

			else if (diff[i].slice(0,1) == "+") {
				chunk.lines.push({
					"status": "addition",
					"text": diff[i].slice(1),
					"preline": null,
					"postline": (postline++)
				});
			}

			else if (diff[i] == "\\ No newline at end of file") {
			}

			else if (!diff[i].length) {
				console.log("Empty line in diff")
			}
			else
				return next(APIError.notImplemented("Error: Not implemented diff format."));

			i++;
		} while (i < diff.length);

		chunks.push(chunk);
		return chunks;
	})
	.then(chunks => { return res.status(200).send(APISuccess.UsercontentDiffItem(chunks)); })
    .catch(next); // Handle throwed errors with express error middleware
};
