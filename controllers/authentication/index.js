const config 			= require("../../config.js");
const lib			= require('gitarchive_lib');
const APIError			= lib.error;
const logger 			= lib.logger;

const AuthenticationModel = require('../../models/authentication');

module.exports = function (req, res, next) {

	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");

	const requestAuthHeader = (req.headers["authorization"] ? req.headers["authorization"] : null);

	if (!requestAuthHeader) {
		res.header("WWW-Authenticate", "Basic realm=\"Please provide a valid API key as username (no password required).\"");

		return next(APIError.unauthorized("Authentication required."));
	}

	const requestAuth = requestAuthHeader.split(' ');

	if (requestAuth.length !== 2)
		{ return next(APIError.unauthorized("Authentication failed.")); }

	const requestAuthScheme = requestAuth[0];
	const requestAuthCredentials = Buffer.from(requestAuth[1], 'base64').toString('utf-8');

	const requestAuthApikey = requestAuthCredentials.split(':')[0];

	if (requestAuthScheme !== "Basic")
		{ return next(APIError.unauthorized("Authentication failed.")) }

	if (!AuthenticationModel.validateApiKey(requestAuthApikey))
		{ return next(APIError.unauthorized("Authentication failed.")) }

	if (req.method === "OPTIONS")
		return res.status(200).end();
	else
		next();
};
