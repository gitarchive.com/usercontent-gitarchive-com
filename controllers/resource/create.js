/**
 * Create a resource within usercontent.
 *
 * This script creates body and headers directories and repositories,
 * for a specific resource. But the file `data` is not created here.
 */

const path = require('path');
const fs = require('fs')
const lib = require('gitarchive_lib');

const APIError = lib.error;
const APISuccess = lib.success;
const Git = lib.git;
const ENV_PATH = require('../../etc/path');

module.exports = async function (req, res, next) {

    const resource = {};
    resource.id = req.params.resource_id;

    if (!resource.id)
    { return next(APIError.badRequest('Resource\'s ID is missing.')); }

    if (resource.id.length !== 64)
        { return next(APIError.badRequest('Resource\'s ID is malformed.')); }

    resource.path = path.join(ENV_PATH.USER_CONTENT_PATH, resource.id);

    // Create the resource's directory (body).
    const mkdirRepo = await mkdir(resource.path)
    if (!mkdirRepo) { return next(APIError.badImplementation('Something failed while trying to make repository directory.')); }

    // Initialize body repository
    const initRepo = await initGit(resource.path)
    if (!initRepo) { return next(APIError.badImplementation("Unable to init repository.")); }

	// Return UsercontentResourceItem object
    return res.send(APISuccess.UsercontentResourceItem(resource.id));
};

/**
 * Promise wrapper to make a directory
 *
 * @param {*} dirPath
 * @returns {true} for success. {false} for failure.
 */

async function mkdir (dirPath) {

    return new Promise(function (resolve, reject) {

        // Create dir
        fs.mkdir(dirPath, function (error) {
            if (error) { return reject(error); }

            return resolve(dirPath);
        })
    })
    .then(() => { return true; })
    .catch((err) => { return RegExp('EEXIST').test(err.message.toString()); });
};

/**
 * Git initializer
 *
 * @param {*} dirPath
 * @returns {true} for success. {false} for failure.
 */

function initGit (dirPath) {

    return new Promise(async function (resolve, reject) {

        // Initialize repo
        const bodyRepo = new Git(dirPath);
        const stdio = await bodyRepo.init();

        // Handle standard output/error
        if (stdio.stderr !== '')
            { return reject('Unable to init Git'); }

        resolve(stdio);
    })
    .then(() => { return true; })
    .catch((err) => { return false; })
};
