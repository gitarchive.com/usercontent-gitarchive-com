/**
 * 
 */

const CreateResource = require("./create");
const CommitResource = require("./commit");


module.exports = {

    create: CreateResource,
    commit: CommitResource

};