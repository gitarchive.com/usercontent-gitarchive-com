/**
 * Make a new commit on a resource's repository.
 * 
 * @param resource_id (mandatory),
 * @param from_commit_id (mandatory),
 * @param to_commit_id (mandatory)
 * @return UsercontentCommitItem (json)
 */

const path = require('path');
const lib = require('gitarchive_lib');

const APIError = lib.error;
const APISuccess = lib.success;
const Git = lib.git;
const ENV_PATH = require('../../etc/path');

module.exports = async function (req, res, next) {

    const resource = {};
    resource.id = req.params.resource_id;

    if (!resource.id)
        { return next(APIError.badRequest('Resource\'s ID is missing.')); }

    if (resource.id.length !== 64)
        { return next(APIError.badRequest('Resource\'s ID is malformed.')); }
    
    resource.path = path.join(ENV_PATH.USER_CONTENT_PATH, resource.id);
    
    const repo = new Git(resource.path);
    const gitStatus = await repo.status();
    
    if (!gitStatus.stdout.match('nothing to commit, working tree clean'))
    {
        await repo.add();
        await repo.commit(new Date());
    }

    const headHash = await repo.headHash();
    const commitHash = headHash.stdout.split("\n")[0];
	
	if (!commitHash || !commitHash.length)
		{ return next(APIError.badImplementation('Commit id is missing.')); }
	
	if (commitHash.length !== 40)
		{ return next(APIError.badImplementation('Commit id is malformed.')); }

    return res.send(APISuccess.UsercontentCommitItem(commitHash));
};
