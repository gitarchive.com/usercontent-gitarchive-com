/**
 * Get raw from a commit on a resource's repository.
 * 
 * @param: 	resource_id (mandatory),
 * 			commit_id (mandatory)
 * 
 * @return: diff object (json)
 */

const path = require('path');
const lib = require('gitarchive_lib');

const APIError = lib.error;
const Git = lib.git;
const ENV_PATH = require('../../etc/path');

module.exports = function (req, res, next, rawType) {

    const resource_id = req.params.resource_id;
    const commit_id = req.params.commit_id;

    // Validate resource & commit params
    const hashFormat = new RegExp("^[A-z0-9]{20,}$");
    
    if (!hashFormat.test(resource_id))
        { return next(APIError.badRequest("Resource's id is malformed.")); }
    if (!hashFormat.test(commit_id))
        { return next(APIError.badRequest("Commit's id is malformed.")); }
    
    // 1. Load repo
    // 2. Fetch raw data from a specific commit.
    // 3. Return raw data if everything ok.
    // 4. Return error with next() otherwise :|
    
    const repoPath = path.join(ENV_PATH.USER_CONTENT_PATH, resource_id);

    (new Promise (function(resolve, reject) {
        const repo = new Git(repoPath);

        // TODO. Confirm that reject error is well-handled.
        if (!repo) { reject(APIError.notFound("Resource was not found.")); }
        else { resolve(repo); };
    }))
    .then((repo) => { return repo.raw(commit_id, rawType == "headers" ? "headers" : "body"); })
    .then((stdio) => {
        
        if (stdio.stderr && new RegExp("not a git repository").test(stdio.stderr))
            { throw APIError.notFound("Resource was not found."); }
        else if (stdio.stderr && new RegExp("Path 'data' does not exist in").test(stdio.stderr))
            { throw APIError.notFound("Resource's commit was not found."); }
        else if (stdio.stderr && new RegExp("Invalid object name").test(stdio.stderr))
            { throw APIError.notFound("Resource's commit was not found."); }
        else if (stdio.stderr && stdio.stderr !== "")
            { console.log(stdio.stderr); throw APIError.notImplemented("Unhandled error with git-raw."); }
        else {

            // Set response headers
            if (rawType == "headers")
                { res.set("Content-type", "text/plain"); }
            else {
                // As per RFC-7231, Content-Type is not mandatory.
                // Though, usercontent does not set a content-type
                // and will let api.gitarchive.com set one.
            }
            
            // Return raw data
            return res.status(200).send(stdio.stdout);
        }
    })
    .catch(next); // Handle throwed errors with express error middleware

};
