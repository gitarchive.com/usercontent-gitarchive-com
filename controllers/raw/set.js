/**
 * Set raw data on a resource's repository.
 * 
 * @param: 	resource_id (mandatory),
 * 			commit_id (mandatory)
 * 
 * @return: diff object (json)
 */

const path		= require('path');
const fs		= require('fs');
const lib		= require('gitarchive_lib');

const ENV_PATH		= require('../../etc/path');
const APIError		= lib.error;
const logger		= lib.logger;

module.exports = function (req, res, next, rawType) {
	const resource_id = req.params.resource_id;

	// Validate resource & commit params
	const hashFormat = new RegExp("^[A-z0-9]{20,}$");

	if (!hashFormat.test(resource_id))
		{ return next(APIError.badRequest("Resource's id is malformed.")); }

	// Create a writable stream
	const writeStream = fs.createWriteStream(path.join(ENV_PATH.USER_CONTENT_PATH, resource_id, (rawType == "headers" ? "headers" : "body")));
	writeStream.on('error', function (error) {
		logger.error("can't create writable stream:", error);

		if (error.errno === -2)
			{ return next(APIError.notFound('Resource was not found on this usercontent.')); }
		else
			{ return next(APIError.badImplementation('Unable to save data.')); }
	});
	writeStream.on('finish', () => { return res.send(); });
	writeStream.on('pipe', () => { console.log("piping"); });

	// Pipe raw data into writable stream
	const pipeStream = req.pipe(writeStream);
};
