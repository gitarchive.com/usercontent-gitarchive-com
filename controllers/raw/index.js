/**
 * Controller to handle raw data of resources.
 * 
 * @param:  resource_id (mandatory),
 *          commit_id (mandatory)
 * @output: raw (not a json)
 */

const getRaw = require('./get');
const setRaw = require('./set');

module.exports = {

    /**
     * Return raw data of a commit from a resource's body.
     * 
     * @param:  resource_id (mandatory),
     *          commit_id (mandatory)
     * @output: raw (not a json)
     */

    getRawBody: function (req, res, next) {

        return getRaw(req, res, next, "body");
    },

    /**
     * Return raw data of a commit from a resource's headers.
     * 
     * @param:  resource_id (mandatory),
     *          commit_id (mandatory)
     * @output: raw (not a json)
     */

    getRawHeaders: function (req, res, next) {

        return getRaw(req, res, next, "headers");
    },

    /**
     * Set raw data of a resource's headers.
     * 
     * @param: resource_id (mandatory)
     * @input: raw headers
     * @output: 
     */

    setRawHeaders: function (req, res, next) {

        return setRaw(req, res, next, "headers");
    },

    /**
     * Set raw data of a resource's body.
     * 
     * @param: resource_id (mandatory)
     * @input: raw body
     * @output: 
     */

    setRawBody: function (req, res, next) {

        return setRaw(req, res, next, "body");
    },

};
