const APIkeys = require('./keys');

module.exports = {

	validateApiKey: function (api_key) {

		return (APIkeys.indexOf(api_key) >= 0);
	}

};
