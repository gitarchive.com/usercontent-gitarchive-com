const path = require('path');

module.exports = {
  VERBOSE: true,
  
  USER_CONTENT_PATH: path.join(__dirname, '..', 'usercontent'),
  USER_CONTENT_BODY_PATH: path.join(__dirname, '..', 'usercontent', 'body'),
  USER_CONTENT_HEADERS_PATH: path.join(__dirname, '..', 'usercontent', 'headers'),

  // CRAWL_RATE: 800, // Time between two requests (in milliseconds) (~ How many requests per second)
}
