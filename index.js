/**
 * Entry point for usercontent.gitarchive.com's API.
 * 
 */

const express			= require("express");
const bodyParser		= require("body-parser");
const lib			= require("gitarchive_lib")
const app			= express();
const Sentry 			= require('@sentry/node');

const config			= require("./config");
const APIError			= lib.error;
const ErrorController		= lib.errorController;
const logger			= lib.logger;

const AuthenticationController = require("./controllers/authentication");
const ResourceController	= require('./controllers/resource');
const DiffController		= require('./controllers/diff');
const RawController		= require('./controllers/raw');

let router = express.Router(); // Create an instance of the express Router

/**
 * Initialize Sentry
 * more: https://docs.sentry.io/platforms/node/express/
 */

Sentry.init({ dsn: config.services.sentry });
app.use(Sentry.Handlers.requestHandler());

/**
 * Log all requests
 */

app.use((req, res, next) => {

	const remoteAddress = (req.header('X-Forwarded-For') || '').split(',').pop()
		|| req.hostname
		|| req.headers.host
		|| req.connection.remoteAddress
		|| req.socket.remoteAddress
		|| req.connection.socket.remoteAddress;

	const remoteUser = req.header('Authorization');
	const userAgent = req.header('User-Agent');

	// ::1 - alpha_f5ad77e32e9126fffa981d218a4ddd1d36fa692b [22/Nov/2018:13:50:38 +0000] "GET / HTTP/1.1" 404 59 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
	logger.info([remoteAddress, remoteUser, ("\"" + req.method + " " + req.originalUrl + "\""), "\"" + userAgent + "\""].join(" "));

	return next();
})

/*
 *	Return current date
 */

function getDate() {
	let now = new Date().getTime() + (2 * 60 * 60 * 1000);
	return ("(" + new Date(now).toISOString().replace(/T/, ' ').replace(/\..+/, '') + ")");
}

/**
 *	Authenticate requests
 */

app.all("*", AuthenticationController);

/**
 *	Parse body on certain queries
 */

// Apply to request where Content-Type is set as "application/octet-stream" (binary)
// This way, req.body is a buffer (useful for UsercontentModel)
// More at https://github.com/expressjs/body-parser/blob/master/README.md#bodyparserrawoptions

//app.use(bodyParser.json({limit: "50mb"}));
//app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

app.use(bodyParser.raw({type: "application/octet-stream", limit: '10mb' }));

// Parse request with Content-Type is set as "application/x-www-form-urlencoded"
// This allow us to use req.body.params :)
// More at https://github.com/expressjs/body-parser/blob/master/README.md#bodyparserrawoptions
app.use(bodyParser.urlencoded({extended: true, type: "application/x-www-form-urlencoded"}));

/**
 *	Surcharge express with a response.success method
 */

app.response.success = function (json) {

	if (typeof json !== "object")
		throw Error("Unable to use res.success, not a json.");

	json.message = (typeof json.message == "string" ? json.message : "succeeded");
	json.statusCode = (typeof json.statusCode == "number" ? json.statusCode : 200);

	this.status(json.statusCode);
	this.json(json);
}

/**
 *	Sanity check route
 */

router.get("/", (req, res) => { return res.status(200).send({message: "UserContent.GitArchive.com API ready - (Version: " + config.services.usercontent.version + ")"}); });

router.post("/resources/:resource_id/raw/headers", RawController.setRawHeaders);
router.put("/resources/:resource_id", ResourceController.create);

router.post("/resources/:resource_id/commits", ResourceController.commit);

router.get("/resources/:resource_id/diff/:from_commit_id\.\.:to_commit_id/body", DiffController.getBody);
router.get("/resources/:resource_id/diff/:from_commit_id\.\.:to_commit_id/headers", DiffController.getHeaders);

router.get("/resources/:resource_id/raw/:commit_id/body", RawController.getRawBody);
router.get("/resources/:resource_id/raw/:commit_id/headers", RawController.getRawHeaders);

router.post("/resources/:resource_id/raw/body", RawController.setRawBody);

/*
 *	Config Git
 */

(async () => {
	const Git = lib.git;
	
	const NoRepo = new Git();
	
	await NoRepo.config('user.name', 'GitBot');
	await NoRepo.config('user.email', 'hello@gitarchive.com')
})();

/**
 *	Config
 */

app.set('port', config.services.usercontent.port);
app.use("/v" + config.services.usercontent.version, router); // Prefix of routes
app.listen(app.get('port')); // Listen port

logger.info("Server started and connected ! API listen on port: " + app.get('port'));

/**
 * Error handling
 */

//
// 404 responses
// more: expressjs.com/en/starter/faq.html#how-do-i-handle-404-responses
//

app.use((req, res, next) => { throw APIError.notFound('Path not found.'); });

//
// Sentry error handler must be before any other error middleware
// more: https://docs.sentry.io/platforms/node/express/
//

app.use(Sentry.Handlers.errorHandler());

//
// Error handler
//

app.use(ErrorController);
